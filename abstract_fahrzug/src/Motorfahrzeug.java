import java.awt.*;

public abstract class Motorfahrzeug {
    private String marke;
    private int leistung;
    private int sitzPlaetze;
    private int zylinder;
    private Color farbe;
    private int space;



    public Motorfahrzeug(String marke, int leistung, int sitzPlaetze, int zylinder, Color farbe, int space) {
        this.marke = marke;
        this.leistung = leistung;
        this.sitzPlaetze = sitzPlaetze;
        this.zylinder = zylinder;
        this.farbe = farbe;
        this.space = space;

    }
    public void setSpace(int space) {
        this.space = space;
    }

    public int getSpace() {
        return space;
    }


    public void print(){
        System.out.println("_________________________________________");
        System.out.println( marke +" "+ leistung +" "+ sitzPlaetze +" "+ zylinder +" "+ farbe);

    }
}

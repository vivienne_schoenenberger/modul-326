import java.awt.*;

public class Motorrad extends Motorfahrzeug{

    private String typ;

    public Motorrad(String marke, int leistung, int sitzPlaetze, int zylinder, Color farbe, String typ,int space) {
        super(marke, leistung, sitzPlaetze, zylinder, farbe,space);
        this.typ = typ;

    }

    public String getTyp() {
        return typ;
    }

    public void setTyp(String typ) {
        this.typ = typ;
    }

    @Override
    public void print() {
        super.print();
        System.out.println(" "+ typ);
    }
}

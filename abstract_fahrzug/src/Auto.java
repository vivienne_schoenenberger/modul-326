import java.awt.*;

public class Auto extends Motorfahrzeug{

    private int tueren;

    public Auto(String marke, int leistung, int sitzPlaetze, int zylinder, Color farbe, int tueren,int space) {
        super(marke, leistung, sitzPlaetze, zylinder, farbe, space);
        this.tueren = tueren;
    }

    public int getTueren() {
        return tueren;
    }

    public void setTueren(int tueren) {
        this.tueren = tueren;
    }

    @Override
    public void print() {
        super.print();
        System.out.println(" " +tueren);
    }
}

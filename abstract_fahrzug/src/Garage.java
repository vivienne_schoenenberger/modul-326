import java.util.ArrayList;

public class Garage {


    private ArrayList<Motorfahrzeug> fahrzeuge;
    private int max = 20;
    private int available = max;
    private int used = 0;



    public Garage() {
        this.fahrzeuge = new ArrayList<>();
    }
    public ArrayList<Motorfahrzeug> getFahrzeuge() {
        return fahrzeuge;
    }

    public int getAvailable() {
        return available;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }
    boolean addFahrzeug(Motorfahrzeug fahrzeug) {

        if (available > fahrzeuge.size()) {
            fahrzeuge.add(fahrzeug);
            available = available - fahrzeug.getSpace();
            used = fahrzeug.getSpace();
            return true;
        } else {
            return false;
        }
    }

    public int getUsed() {
        return used;
    }

    public void printAllFahrzeuge(){
        fahrzeuge.forEach(Motorfahrzeug::print);
        System.out.println(available);
    }
}
